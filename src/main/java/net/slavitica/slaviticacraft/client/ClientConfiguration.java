/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ClientConfiguration {

    String javaExecutablePath;
    int memoryMinimum;
    int memoryMaximum;
    int memoryPermSize;
    boolean ignorePatchDiscrepencies;
    boolean ignoreInvalidMinecraftCertificates;
    String logLevel;
    File libraryPath;
    List<File> classPathList;
    String mainClassName;
    String username;
    Token token;
    String version;
    File gameDirectory;
    File assetDirectory;
    int width;
    int height;
    String tweakClassName;
    public static final String MEMORY_MIN_FORMAT;
    public static final String MEMORY_MAX_FORMAT;
    public static final String MEMORY_PERM_FORMAT;

    static {
        MEMORY_MIN_FORMAT = "-Xms%dM";
        MEMORY_MAX_FORMAT = "-Xmx%dM";
        MEMORY_PERM_FORMAT = "-XX:PermSize=%dM";
    }

    public ClientConfiguration() {
        javaExecutablePath = null;
        memoryMinimum = 256;
        memoryMaximum = 1024;
        memoryPermSize = 256;

        ignorePatchDiscrepencies = true;
        ignoreInvalidMinecraftCertificates = true;
        logLevel = "INFO";

        libraryPath = null;
        classPathList = null;

        mainClassName = "net.minecraft.Launcher";
        username = null;
        token = null;
        version = null;

        gameDirectory = null;
        assetDirectory = null;

        width = 854;
        height = 480;

        tweakClassName = "cpw.mods.fml.common.launcher.FMLTweaker";
    }

    public String getJavaExecutablePath() {
        return javaExecutablePath;
    }

    public void setJavaExecutablePath(String javaExecutablePath) {
        this.javaExecutablePath = javaExecutablePath;
    }

    public int getMemoryMinimum() {
        return memoryMinimum;
    }

    public void setMemoryMinimum(int memoryMinimum) {
        this.memoryMinimum = memoryMinimum;
    }

    public int getMemoryMaximum() {
        return memoryMaximum;
    }

    public void setMemoryMaximum(int memoryMaximum) {
        this.memoryMaximum = memoryMaximum;
    }

    public int getMemoryPermSize() {
        return memoryPermSize;
    }

    public void setMemoryPermSize(int memoryPermSize) {
        this.memoryPermSize = memoryPermSize;
    }

    public boolean isIgnorePatchDiscrepencies() {
        return ignorePatchDiscrepencies;
    }

    public void setIgnorePatchDiscrepencies(boolean ignorePatchDiscrepencies) {
        this.ignorePatchDiscrepencies = ignorePatchDiscrepencies;
    }

    public boolean isIgnoreInvalidMinecraftCertificates() {
        return ignoreInvalidMinecraftCertificates;
    }

    public void setIgnoreInvalidMinecraftCertificates(boolean ignoreInvalidMinecraftCertificates) {
        this.ignoreInvalidMinecraftCertificates = ignoreInvalidMinecraftCertificates;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public File getLibraryPath() {
        return libraryPath;
    }

    public void setLibraryPath(File libraryPath) {
        this.libraryPath = libraryPath;
    }

    public List<File> getClassPathList() {
        return classPathList;
    }

    public void setClassPathList(List<File> classPathList) {
        this.classPathList = classPathList;
    }

    public String getMainClassName() {
        return mainClassName;
    }

    public void setMainClassName(String mainClassName) {
        this.mainClassName = mainClassName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public File getGameDirectory() {
        return gameDirectory;
    }

    public void setGameDirectory(File gameDirectory) {
        this.gameDirectory = gameDirectory;
    }

    public File getAssetDirectory() {
        return assetDirectory;
    }

    public void setAssetDirectory(File assetDirectory) {
        this.assetDirectory = assetDirectory;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getTweakClassName() {
        return tweakClassName;
    }

    public void setTweakClassName(String tweakClassName) {
        this.tweakClassName = tweakClassName;
    }

    public List<String> getProcessBuilderArguments() {
        List<String> arguments = new ArrayList<String>();

        arguments.add(getJavaExecutablePath());
        arguments.add(String.format(MEMORY_MIN_FORMAT, getMemoryMinimum()));
        arguments.add(String.format(MEMORY_MAX_FORMAT, getMemoryMaximum()));
        arguments.add(String.format(MEMORY_PERM_FORMAT, getMemoryPermSize()));
        arguments.add("-Dfml.ignorePatchDiscrepancies=" + isIgnorePatchDiscrepencies());
        arguments.add("-Dfml.ignoreInvalidMinecraftCertificates=" + isIgnoreInvalidMinecraftCertificates());
        arguments.add("-Dfml.log.level=" + getLogLevel());
        arguments.add("-Djava.library.path=" + getLibraryPath());

        StringBuilder classPathBuilder = new StringBuilder();
        List<File> classPathListing = getClassPathList();
        String classPathSeparator = System.getProperty("path.separator", ";");
        
        if (classPathListing != null && classPathListing.size() > 0) {
            for(int i=0;i<classPathListing.size()-1;i++) {
                File classPath = classPathListing.get(i);
                classPathBuilder.append(classPath.getAbsoluteFile()).append(classPathSeparator);
            }
            classPathBuilder.append(classPathListing.get(classPathListing.size()-1).getAbsoluteFile());
        }
        
        arguments.add("-cp");
        arguments.add(classPathBuilder.toString());
        arguments.add(getMainClassName());
        arguments.add("--username");
        arguments.add(getUsername());
        arguments.add("--session");
        arguments.add((getToken() != null) ? getToken().toString() : (Token.OFFLINE_TOKEN).toString());
        arguments.add("--version");
        arguments.add(getVersion());
        arguments.add("--gameDir");
        arguments.add((getGameDirectory() != null) ? getGameDirectory().getAbsolutePath() : ".");
        arguments.add("--assetsDir");
        arguments.add((getAssetDirectory() != null) ? getAssetDirectory().getAbsolutePath() : ".");
        arguments.add("--width=" + getWidth());
        arguments.add("--height=" + getHeight());
        arguments.add("--tweakClass=" + getTweakClassName());
        return arguments;
    }

    @Override
    public String toString() {
        StringBuilder configurationStringBuilder = new StringBuilder();

        List<String> arguments = getProcessBuilderArguments();
        for (String argument : arguments) {
            if(argument.contains(" ")) {
                argument = "\"" + argument + "\"";
            }
            configurationStringBuilder.append(argument).append(' ');
        }

        String outputString = configurationStringBuilder.toString();
        return (outputString.length() > 1) ? outputString.substring(0, outputString.length()-1) : "";
    }
}
