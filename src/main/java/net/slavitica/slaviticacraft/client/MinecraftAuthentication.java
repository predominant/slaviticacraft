/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client;

import cc.plural.jsonij.JPath;
import cc.plural.jsonij.JSON;
import cc.plural.jsonij.parser.ParserException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MinecraftAuthentication {

    public static final String OLD_ACCOUNT_URL = "https://login.minecraft.net/?user=%s&password=%s&version=999";
    public static final String NEW_ACCOUNT_URL = "https://authserver.mojang.com/authenticate";
    public static final String NEW_ACCOUNT_REQUEST = "{\"agent\":{\"name\":\"Minecraft\",\"version\":10},\"username\":\"%s\",\"password\":\"%s\",\"clientToken\":\"%s\"}";

    public static Token authenticateToken(String username, String password) throws AuthenticationException {
        JSON responseJSON = authenticate(username, password);
        String accessToken = "0";
        String clientToken = "0";
        String profileID = "0";
        String profileName = "Offline";
        try {
            accessToken = JPath.evaluate(responseJSON, "/accessToken").getString();
            clientToken = JPath.evaluate(responseJSON, "/clientToken").getString();
            profileID = JPath.evaluate(responseJSON, "/selectedProfile/id").getString();
            profileName = JPath.evaluate(responseJSON, "/selectedProfile/name").getString();
        } catch (ParserException ex) {
            Logger.getLogger(MinecraftAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MinecraftAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Token(accessToken, clientToken, profileID, profileName);
    }

    public static JSON authenticate(String username, String password) throws AuthenticationException {
        try {
            URL url = new URL(NEW_ACCOUNT_URL);
            String request = String.format(NEW_ACCOUNT_REQUEST, username, password, UUID.randomUUID());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length", "" + request.getBytes().length);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.write(request.getBytes());
            writer.flush();
            writer.close();

            int responseCode = connection.getResponseCode();
            switch (responseCode) {
                case HttpURLConnection.HTTP_FORBIDDEN:
                    throw new AuthenticationException("" + HttpURLConnection.HTTP_FORBIDDEN + " " + connection.getURL());
                default:
                    InputStream responseStream = connection.getInputStream();
                    InputStreamReader responseReader = new InputStreamReader(responseStream);
                    return JSON.parse(responseReader);
            }
        } catch (IOException ex) {
            Logger.getLogger(MinecraftAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            throw new AuthenticationException("Error Connecting", ex);
        } catch (ParserException ex) {
            Logger.getLogger(MinecraftAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            throw new AuthenticationException("Error Parsing Response", ex);
        }
    }
}
