/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.slavitica.slaviticacraft.client.AuthenticationException;
import net.slavitica.slaviticacraft.client.ClientConfiguration;
import net.slavitica.slaviticacraft.client.MinecraftAuthentication;
import net.slavitica.slaviticacraft.client.Token;

public class App {

    public static final boolean DEBUG;

    static {
        DEBUG = true;
    }

    public static void main(String[] args) throws AuthenticationException, IOException {
        System.out.println("SlaviticaCraft 0.0.1");
        System.out.print("Username:");
        String user = System.console().readLine();
        System.out.print("Password:");
        String password = new String(System.console().readPassword());

        System.out.println("Connecting User:" + user + "");

        Token token = MinecraftAuthentication.authenticateToken(user, password);
        
        System.out.println("Authenticated!");
        
        File baseDirectory = new File("./Instance");
        File binDirectory = new File(baseDirectory, "bin");
        File nativeDirectory = new File(binDirectory, "natives");
        File jarMods = new File(baseDirectory, "jarmods");
        File modDirectory = new File(baseDirectory, "mods");
        File configDirectory = new File(baseDirectory, "config");

        File resourcesDirectory = new File("./Resources");
        
        List<File> jarList = new ArrayList<File>();
        if (binDirectory.exists()) {
            File[] files = binDirectory.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    continue;
                }
                jarList.add(file);
            }
        }
        if (jarMods.exists()) {
            File[] files = jarMods.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    continue;
                }
                jarList.add(file);
            }
        }

        ClientConfiguration config = new ClientConfiguration();
        
        String javaHomePath = System.getProperty("java.home");
        if(javaHomePath == null) {
            System.out.println("Set a Java Home F000L!");
            System.exit(0);
        }
        
        File javaHome = new File(javaHomePath);
        File javaBin = new File(javaHome, "bin");
        File javaBinary = null;
        
        String osName = System.getProperty("os.name");
        
        if(osName.startsWith("Windows")) {
            javaBinary = new File(javaBin, "javaw.exe");
        } else {
            javaBinary = new File(javaBin, "java");
        }
       
        
        config.setJavaExecutablePath(javaBinary.getAbsolutePath());
        config.setLibraryPath(nativeDirectory);
        config.setClassPathList(jarList);
        config.setUsername(token.getProfileName());
        config.setToken(token);
        config.setMainClassName("net.minecraft.launchwrapper.Launch");
        config.setVersion("1.6.4");
        config.setGameDirectory(baseDirectory);
        config.setAssetDirectory(resourcesDirectory);

        List<String> arguments = config.getProcessBuilderArguments();

        ProcessBuilder processBuilder = new ProcessBuilder(arguments);
        processBuilder.redirectErrorStream(true);
        Process process = processBuilder.start();

        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);

        String line;

        System.out.printf("Output of running %s is:", Arrays.toString(args));

        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        System.out.println("Nothing to see here." + config.toString());

    }
}